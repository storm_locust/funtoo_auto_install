#!/sbin/env python

import logging
import os
import subprocess
from subprocess import PIPE

from download import download

cPath = os.getcwd()
root_password = "123qweasd"

hostname = "IPC101"

stage_url = 'https://build.funtoo.org/1.3-release-std/x86-64bit/generic_64/stage3-latest.tar.xz'
src_list = []

boot_disk = "/dev/sda"
boot_src = "/dev/sda1"
home_src = "/dev/sda4"
root_src = "/dev/sda3"
swap = "/dev/sda2"

src_list.append(root_src)
src_list.append(home_src)
src_list.append(boot_src)

dst_list = []
root_dst = "/mnt/funtoo"
boot_dst = os.path.join(root_dst, "boot")
home_dst = os.path.join(root_dst, "home")

dst_list.append(root_dst)
dst_list.append(boot_dst)
dst_list.append(home_dst)

cmd_root = 'env -i HOME=/root TERM=$TERM /bin/chroot {root_dst} /bin/bash -c'.format(root_dst=root_dst)

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s : %(message)s',
                    filename='install_log.txt')


def myprint(*args):
    if len(args) > 1:
        data = ",".join(args)
    else:
        data = args[0]
    logging.info(data)
    print(data)


def mount_disk(src, dst):
    myprint("mount {} {}".format(src_list[src], dst))
    os.system("mount {} {}".format(src_list[src], dst))


def run_cmd(cmd):
    cmd = '{} "source /etc/profile && {}"'.format(cmd_root, cmd)
    myprint("Run: {}".format(cmd))
    os.system(cmd)


def auto_disk():
    for i, v in enumerate(dst_list):
        if not os.path.exists(v):
            myprint("Generate folder: {}".format(v))
            os.system("mkdir {}".format(v))
        else:
            myprint("folder exists: {}".format(v))
        mount_disk(i, v)
    os.system("swapon {}".format(swap))


def extract_stage():
    os.chdir(root_dst)
    os.system("tar xvf {}".format(os.path.basename(stage_url)))


def mount_device():
    os.chdir(root_dst)
    mount_list = ['mount -t proc none proc',
                  'mount --rbind /sys sys',
                  'mount --rbind /dev dev']
    for m in mount_list:
        os.system("{}".format(m))
    os.system('cp /etc/resolv.conf /mnt/funtoo/etc/')


def write_to_file(target, data):
    with open(target, "a") as files:
        files.write('{}\n'.format(data))


def run_cmd_subprocess(cmd, single_res=False):
    cmd = '{} "{}"'.format(cmd_root, cmd)
    pn = subprocess.Popen(cmd, shell=True, stdout=PIPE, stdin=PIPE, stderr=PIPE)
    lines = []
    while True:
        line = pn.stdout.readline()
        if not line:
            break
        else:
            myprint("run_cmd_subprocess:", line.strip().decode())
            if single_res:
                return line.strip().decode()
            else:
                lines.append(line.strip().decode())

        err_line = pn.stderr.readline()
        if err_line:
            myprint("run_cmd_subprocess:", line.strip().decode())
            if single_res:
                return line.strip().decode()
    return lines


def sync_set_upgrade():
    myprint("copy fstab to new system...")
    myprint("cp {} {}".format(os.path.join(cPath, "fstab"), os.path.join(root_dst, "etc/")))
    os.system("cp {} {}".format(os.path.join(cPath, "fstab"), os.path.join(root_dst, "etc/")))


def first_update():
    with open("source_hostname", "r") as files:
        bconf = files.readlines()

    for i, b in enumerate(bconf):
        if "hostname=" in b:
            bconf[i] = b.replace('hostname="localhost"', 'hostname="{}"'.format(hostname))

    with open("{}".format(os.path.join(root_dst, "etc/conf.d/hostname")), "w") as files:
        for t in bconf:
            files.write(t)
    run_cmd("ln -sf /usr/share/zoneinfo/Asia/Taipei /etc/localtime")
    write_to_file("{}".format(os.path.join(root_dst, "etc/portage/make.conf")),
                  'MAKEOPTS="-j{}"'.format(int(run_cmd_subprocess('nproc', single_res=True)) + 1))
    run_cmd(
        "cd /etc/init.d && ln -s netif.tmpl net.eth0 && rc-update add net.eth0 default && echo template=dhcpcd > /etc/conf.d/net.eth0 && openrc")
    run_cmd("ego sync")
    run_cmd("emerge -uDN @world")


def install_kernel():
    run_cmd('echo \\"sys-kernel/debian-sources binary\\" > /etc/portage/package.use')
    run_cmd("emerge -v debian-sources")


def set_password():
    run_cmd('echo -e \\"{}\\n{}\\" | passwd root'.format(root_password, root_password))


def install_pkg(pkg):
    run_cmd("emerge -uv {}".format(pkg))


def set_service_default():
    run_cmd("rc-update add sshd default")


def install_mbr():
    boot_info = run_cmd_subprocess("ls /boot/ | grep sources-x86_64 | grep -v 'System'")
    condition = {'initramfs': 'initramfs[-v]', 'kernel': 'kernel[-v]'}
    with open("source_boot.conf", "r") as files:
        bconf = files.readlines()

    for aa in boot_info:
        for i, b in enumerate(bconf):
            if condition[aa.split("-")[0]] in b:
                bconf[i] = bconf[i].replace(condition[aa.split("-")[0]], aa)
                break
    with open("boot.conf", "w") as files:
        for t in bconf:
            files.write(t)

    boot_conf_cmd = "cp boot.conf {}".format(os.path.join(root_dst, "etc/"))
    myprint(boot_conf_cmd)
    os.system(boot_conf_cmd)
    run_cmd("grub-install --target=i386-pc --no-floppy {}".format(boot_disk))
    run_cmd("ego boot update")


if __name__ == '__main__':
    auto_disk()
    assert download(url=stage_url, savepath=root_dst), "Download stage fail..."
    extract_stage()
    mount_device()
    sync_set_upgrade()
    first_update()
    install_kernel()
    set_password()
    set_service_default()
    install_pkg("vim")
    install_pkg("grub")
    install_pkg("intel-microcode")
    install_pkg("iucode_tool")
    install_mbr()
